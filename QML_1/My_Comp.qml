import QtQuick 2.0

Rectangle{
    property int comWidth: 200
    property int comHeight: 200
    property color comColor: "red"
    property int comRadius: 100

    width: comWidth
    height: comHeight
    color: comColor
    radius: comRadius
}

