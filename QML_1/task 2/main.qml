import QtQuick 2.12
import QtQuick.Window 2.12

Window {
    id: win
    visible: true
    width: 400
    height: 700
    minimumWidth: 200
    minimumHeight: 400
    title: qsTr("Task_For_Layout")
    Item {
        id: root
        anchors.fill: parent
        My_Comp2 {
            id: fon
            anchors.fill: parent
            color: "#f5f5f5"
            comText: ""
        }

        My_Comp2 {
            id: rect1
            anchors.top: parent.top
        }

        My_Comp2 {
            id: rect2
            width: parent.width * 0.9
            height: parent.height - (rect1.height + anchors.margins*2 + rect3.height)
            color: "#f5f5f5"
            border.color: "#d3d3d3"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: rect1.bottom
            anchors.margins: (parent.width - width) / 2
            comText: "Content"
        }

        Item {
            id: root2
            width: parent.width
            height: parent.height * 0.1
            anchors.bottom: parent.bottom

            My_Comp2 {
                id: bottom_rect
                anchors.fill: parent
                color: "#dbdbdb"
                comText: ""
            }

            My_Comp2 {
                id: rect3
                width: (parent.width - 8) / 3
                height: parent.height
                color: "#d3d3d3"
                comText: "1"
            }

            My_Comp2 {
                id: rect4
                width: (parent.width - 8) / 3
                height: parent.height
                color: "#d3d3d3"
                anchors.left: rect3.right
                anchors.leftMargin: 4
                comText: "2"
            }

            My_Comp2 {
                id: rect5
                width: (parent.width - 8) / 3
                height: parent.height
                color: "#d3d3d3"
                anchors.left: rect4.right
                anchors.leftMargin: 4
                comText: "3"
            }
        }
    }
}
