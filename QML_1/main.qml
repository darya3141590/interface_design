import QtQuick 2.12
import QtQuick.Window 2.12

Window {
    id: win
    visible: true
    width: 400
    height: 400
    Item{
        id: root
        anchors.fill: parent

        My_Comp {
            anchors.centerIn: parent

            My_Comp {
                comWidth: 30
                comHeight: 30
                comColor: "white"
                comRadius: 30
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: 30
            }

            My_Comp {
                comWidth: 25
                comHeight: 20
                comRadius: 17
                comColor: "black"
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 30
                My_Comp {
                    comWidth: 10
                    comHeight: 50
                    comRadius: 5
                    comColor: "brown"
                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottomMargin: 5
                }
            }
        }
    }
}
