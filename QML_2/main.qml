import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3

Window {
    id:win
    minimumWidth: layout1.implicitWidth
    minimumHeight: layout1.implicitHeight
    width: 1100; height: 300; visible: true
    title: qsTr("Layouts")
    RowLayout{
        id:layout1
        spacing:10
        My_Comp1{}
        My_Comp1{}
        My_Comp1{}
    }
}
