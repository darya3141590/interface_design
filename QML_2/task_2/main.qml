import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3

Window {
    visible: true
    width: 400
    height: 700
    minimumWidth: 200
    minimumHeight: 400

    Rectangle {anchors.fill: parent; color: "#f5f5f5"}

    ColumnLayout {
        anchors.fill: parent; spacing: (parent.width - rect.width) / 2

        RowLayout {
            My_Comp2 {Layout.fillWidth: true; Layout.preferredHeight: 70; comColor: "#dcdcdc"}
        }

        RowLayout {
            Layout.leftMargin: (parent.width - rect.width) / 2
            My_Comp2 {
                id: rect
                width: parent.width * 0.95; height: parent.height
                comColor: "#f5f5f5"
                comBorderColor: "#d3d3d3"
                comText: "Content"
            }
        }

        RowLayout {
            My_Comp2 {
                Layout.fillWidth: true; Layout.preferredHeight: 70; comText: ""; comColor: "#dbdbdb"
                RowLayout {
                    anchors.fill: parent; spacing: 4
                    My_Comp2 {Layout.fillWidth: true; Layout.fillHeight: true; comText: "1"}
                    My_Comp2 {Layout.fillWidth: true; Layout.fillHeight: true; comText: "2"}
                    My_Comp2 {Layout.fillWidth: true; Layout.fillHeight: true; comText: "3"}
                    My_Comp2 {Layout.fillWidth: true; Layout.fillHeight: true; comText: "4"}
                }
            }
        }
    }
}

