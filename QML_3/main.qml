import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3

Window {
    visible: true
    width: 400
    height: 700
    minimumWidth: 200
    minimumHeight: 400

    function updateContent(headerText, contentText, it, a, b, c) {
        header.comText = headerText; rect.comText = contentText;
        it.opacity = 1; a.opacity = 0.3; b.opacity = 0.3; c.opacity = 0.3
    }

    Rectangle {anchors.fill: parent; color: "#f5f5f5"}

    ColumnLayout {
        anchors.fill: parent; spacing: (parent.width - rect.width) / 2

        RowLayout {
            MyComp2 {id: header; Layout.fillWidth: true; Layout.preferredHeight: 70; comColor: "#dcdcdc"}
        }

        RowLayout {
            Layout.leftMargin: (parent.width - rect.width) / 2
            MyComp2 {
                id: rect
                width: parent.width * 0.95; height: parent.height
                comColor: "#f5f5f5"
                comBorderColor: "#d3d3d3"
                comText: "Content"
            }
        }

        RowLayout {
            MyComp2 {
                Layout.fillWidth: true; Layout.preferredHeight: 70; comText: ""; comColor: "#dbdbdb"
                RowLayout {
                    anchors.fill: parent; spacing: 4
                    MyComp2 {
                        id: bottom1; Layout.fillWidth: true; Layout.fillHeight: true; comText: "1"
                        MouseArea {
                            anchors.fill: parent
                            onPressed: updateContent("Header 1", "Content 1", bottom1, bottom2, bottom3, bottom4)
                        }
                    }
                    MyComp2 {
                        id: bottom2; Layout.fillWidth: true; Layout.fillHeight: true; comText: "2"
                        MouseArea {
                            anchors.fill: parent
                            onPressed: updateContent("Header 2", "Content 2", bottom2, bottom1, bottom3, bottom4)
                        }
                    }
                    MyComp2 {
                        id: bottom3; Layout.fillWidth: true; Layout.fillHeight: true; comText: "3"
                        MouseArea {
                            anchors.fill: parent
                            onPressed: updateContent("Header 3", "Content 3", bottom3, bottom1, bottom2, bottom4)
                        }
                    }
                    MyComp2 {
                        id: bottom4; Layout.fillWidth: true; Layout.fillHeight: true; comText: "4"
                        MouseArea {
                            anchors.fill: parent
                            onPressed: updateContent("Header 4", "Content 4", bottom4, bottom1, bottom2, bottom3)
                        }
                    }
                }
            }
        }
    }
}

