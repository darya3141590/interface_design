import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2

Window {
    width: 360
    height: 640
    visible: true
    title: qsTr("StackView_test")
    property int defMargin:10
    Page {
        id:p
        anchors.fill: parent
        StackView{
            id:stack_view
            anchors.fill: parent
            initialItem: page1
        }
        header:ToolBar{
            id:page_header
            height:40
            RowLayout{
                ToolButton{
                    id:back_btn
                    Text{
                        text: "<-"
                        font.pixelSize: 24
                        visible:stack_view.currentItem != page1
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    onClicked: {
                        if (stack_view.currentItem == page2) {
                            stack_view.replace(page1, StackView.PopTransition)
                        }
                        if (stack_view.currentItem == page3) {
                            stack_view.replace(page2, StackView.PopTransition)
                        }
                    }
                }
                Text{
                    id:header_page_text
                    anchors.centerIn: page_header
                }
            }
        }
        My_Page { id:page1
            backgroundColor: "red"
            buttonText: "To_Green"
            secButtonText: "To_Yellow"
            onButtonClicked: {
                stack_view.replace(page2, StackView.PushTransition)
            }
            onSecButtonClicked: {
                stack_view.replace(page3, StackView.PopTransition)
            }
        }
        My_Page { id:page2
            visible: false
            backgroundColor: "green"
            secButtonText: "To_Red"
            buttonText: "To_Yellow"
            onSecButtonClicked: {
                stack_view.replace(page1, StackView.PopTransition)
            }
            onButtonClicked: {
                stack_view.replace(page3, StackView.PushTransition)
            }
        }
        My_Page { id:page3
            visible: false
            backgroundColor: "yellow"
            secButtonText: "To_Green"
            buttonText: "To_Red"
            onSecButtonClicked: {
                stack_view.replace(page2, StackView.PopTransition)
            }
            onButtonClicked: {
                stack_view.replace(page1, StackView.PushTransition)
            }
        }
    }
}
