import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5

Window {
    width: 360
    height: 640
    visible: true
    title: qsTr("Task_for_ListView_Model")
    property int defMargin: 8
    Page{
        id:page
        anchors.fill:parent

        Rectangle {
            anchors.fill: parent
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#d2d592" }
                GradientStop { position: 1.0; color: "#98bb91" }
            }
        }

        ListModel{
            id:my_model
            ListElement{message: "HI all!"; time: "12:00"}
            ListElement{message: "Vsem privet"; time: "12:10"}
            ListElement{message: "Holla!!!"; time: "12:20"}
        }
        Component{
            id:my_delegate
            MyDelegate{
                message: model.message
                time: model.time
                width: parent.width * 0.9
                height:40
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
        ListView{
            id:my_list
            anchors.fill:parent
            model:my_model
            delegate:my_delegate
            spacing: 10
            anchors.topMargin: (parent.width * 0.1) / 2
        }

        footer: PageFooter {
            onNewMessage: {
                //Append new message
                var newMsg = {};
                newMsg.message = msg;
                newMsg.time = Qt.formatTime(new Date(), "hh:mm");
                my_model.append(newMsg);
            }
        }
    }
}
