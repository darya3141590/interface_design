import QtQuick 2.12

Item {
    id:deleg
    property string message: "?"
    property string time: "?"

    Rectangle{
        id:rect
        border.color:"darkgrey"
        radius:5
        anchors.fill:parent
        gradient:Gradient{
        GradientStop{position:0;color:"lightgray"}
        GradientStop{position:1;color:"white"}
        }
        Row{
            id:row
            anchors.left:parent.left
            anchors.right: parent.right
            height:parent.height
            Text{ text:message; leftPadding: 5; topPadding: 7}
            Text{ text:time; anchors.bottom:
            parent.bottom; anchors.right: parent.right}
        }
    }
}
