import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2

Window {
    visible: true
    width: 580
    height: 580
    minimumHeight: 300
    minimumWidth: 300
    title: qsTr("Task_For_Login_Page")

    Rectangle {
            id: fon
            anchors.fill: parent
            color :"#f5f5f5"

            ColumnLayout {
                width: 0.5 * parent.width
                height: 0.22 * parent.height
                anchors.centerIn: parent

                TextField {
                    id : usernameField
                    placeholderText: "Username"
                    Layout.fillWidth: true
                    Layout.preferredHeight: 30
                    font.pixelSize: 16
                }

                TextField {
                    id: passwordField
                    placeholderText: "Password"
                    Layout.fillWidth: true
                    Layout.preferredHeight: 30
                    font.pixelSize: 16
                    echoMode: TextInput.Password
                }

                RowLayout {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    Button {
                        text: "Log In"
                        Layout.fillWidth: true
                        Layout.preferredHeight: 30
                        background: Rectangle {
                            color: "#e0e0e0"
                        }
                    }

                    Button {
                        text: "Clear"
                        Layout.fillWidth: true
                        Layout.preferredHeight: 30
                        background: Rectangle {color: "transparent"}
                        onClicked: {
                            usernameField.text = ""
                            passwordField.text = ""
                        }
                    }
                }
            }
        }

}
