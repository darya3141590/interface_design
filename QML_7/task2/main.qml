import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2

Window {
    visible: true
    width: 500
    height: 600
    minimumHeight: 400
    minimumWidth: 400
    title: "Task_For_Login_Page"

    Rectangle {
        id: fon
        anchors.fill: parent

        ColumnLayout{
            anchors.centerIn: parent
            width: 0.8 * parent.width
            height: 0.7 * parent.height
            Text {
                text: "Enter your password:"
                font.pixelSize: 16
                Layout.alignment: Qt.AlignHCenter
            }
            Text{
                id : passwordField
                text : passwordField.text
                color: fon.color
            }

            RowLayout {
                spacing: 6
                Layout.alignment: Qt.AlignHCenter

                Repeater {
                    model: 6

                    Label {
                        width: 20
                        height: 20
                        font.pixelSize: 36
                        text: "*"
                        color: index < passwordField.text.length ? "black" : "light grey"
                    }
                }
            }
            GridLayout {
                id: keypad
                rows: 4
                columns: 3
                width: parent.width
                Layout.alignment: Qt.AlignHCenter

                Button {
                    text: "1"
                    onClicked: passwordField.text += "1"
                }
                Button {
                    text: "2"
                    onClicked: passwordField.text += "2"
                }
                Button {
                    text: "3"
                    onClicked: passwordField.text += "3"
                }
                Button {
                    text: "4"
                    onClicked: passwordField.text += "4"
                }
                Button {
                    text: "5"
                    onClicked: passwordField.text += "5"
                }
                Button {
                    text: "6"
                    onClicked: passwordField.text += "6"
                }
                Button {
                    text: "7"
                    onClicked: passwordField.text += "7"
                }
                Button {
                    text: "8"
                    onClicked: passwordField.text += "8"
                }
                Button {
                    text: "9"
                    onClicked: passwordField.text += "9"
                }
                Button {
                    text: ""
                }
                Button {
                    text: "0"
                    onClicked: passwordField.text += "0"
                }
                Button {
                    text: "Clear"
                    onClicked: passwordField.text = ""
                }
                Button {
                    text: "Log In"
                    Layout.topMargin: 10

                }
            }
        }
    }
}

