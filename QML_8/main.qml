import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12


Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Прокручиваемый светофор")

    SwipeView {
        id: view

        currentIndex: 0
        anchors.fill: parent

        MyPage {
            id: firstPage
            comText: "Red Page"
            comColor: "red"
        }

        MyPage {
            id: secondPage
            comText: "Yellow Page"
            comColor: "yellow"
        }

        MyPage {
            id: thirdPage
            comText: "Green Page"
            comColor: "green"
        }
    }
    PageIndicator {
        id: indicator

        count: view.count
        currentIndex: view.currentIndex

        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
