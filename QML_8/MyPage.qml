import QtQuick 2.12
import QtQuick.Controls 2.12

Page {
    property alias comText: text1.text
    property alias comColor: rect1.color
    Rectangle {
        id: rect1
        anchors.fill: parent
        Text {
            id: text1
            anchors.centerIn: parent
            font.pointSize: 15
        }
    }
}
