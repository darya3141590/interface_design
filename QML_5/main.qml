import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3

Window {
    visible: true
    width: 400
    height: 700
    minimumWidth: 200
    minimumHeight: 400

    Rectangle {anchors.fill: parent; color: "#f5f5f5"}

    ColumnLayout {
        anchors.fill: parent; spacing: (parent.width - content.width) / 2


        RowLayout {
            MyComp2 {
                id: header; Layout.fillWidth: true; Layout.preferredHeight: 70; comColor: "#dcdcdc"
                Image {
                    id: arrow; width: 30; height: 30; source: "new/prefix1/Left-Arrow-PNG-Image.png"; anchors.verticalCenter: parent.verticalCenter; anchors.left: parent.left; anchors.leftMargin: 15; opacity: 0;
                }
            }
        }

        RowLayout {
            Layout.leftMargin: (parent.width - content.width) / 2
            MyComp2 {
                id: content
                width: parent.width * 0.95; height: parent.height
                comColor: "#f5f5f5"
                comBorderColor: "#d3d3d3"
                comText: "Content"
            }
        }

        RowLayout {
            MyComp2 {
                Layout.fillWidth: true; Layout.preferredHeight: 70; comText: ""; comColor: "#dbdbdb"
                RowLayout {
                    id: bottom; anchors.fill: parent; spacing: 4

                    PropertyAnimation {id: anim1; target: bottom1; property: "opacity"; to: 0.3; duration: 500}
                    PropertyAnimation {id: anim2; target: bottom2; property: "opacity"; to: 0.3; duration: 500}
                    PropertyAnimation {id: anim3; target: bottom3; property: "opacity"; to: 0.3; duration: 500}
                    PropertyAnimation {id: anim4; target: bottom4; property: "opacity"; to: 0.3; duration: 500}

                    MyComp2 {
                        id: bottom1; Layout.fillWidth: true; Layout.fillHeight: true; comText: "1"
                        MouseArea {
                            anchors.fill: parent
                            onPressed: {anim2.start(); anim3.start(); anim4.start(); bottom.state="first"; arrow.opacity=0}
                        }
                    }
                    MyComp2 {
                        id: bottom2; Layout.fillWidth: true; Layout.fillHeight: true; comText: "2"
                        MouseArea {
                            anchors.fill: parent
                            onPressed: {anim1.start(); anim3.start(); anim4.start(); bottom.state="second"; arrow.opacity=1}
                        }
                    }
                    MyComp2 {
                        id: bottom3; Layout.fillWidth: true; Layout.fillHeight: true; comText: "3"
                        MouseArea {
                            anchors.fill: parent
                            onPressed: {anim1.start(); anim2.start(); anim4.start(); bottom.state="third"; arrow.opacity=1}
                        }
                    }
                    MyComp2 {
                        id: bottom4; Layout.fillWidth: true; Layout.fillHeight: true; comText: "4"
                        MouseArea {
                            anchors.fill: parent
                            onPressed: {anim1.start(); anim2.start(); anim3.start(); bottom.state="forth"; arrow.opacity=1}
                        }
                    }

                    states: [
                        State {
                            name: "first"
                            PropertyChanges {target: bottom2; opacity: 0.3}
                            PropertyChanges {target: bottom3; opacity: 0.3}
                            PropertyChanges {target: bottom4; opacity: 0.3}
                            PropertyChanges {target: header; comText: "Header 1"}
                            PropertyChanges {target: content; comText: "Content 1"}
                        },

                        State {
                            name: "second"
                            PropertyChanges {target: bottom1; opacity: 0.3}
                            PropertyChanges {target: bottom3; opacity: 0.3}
                            PropertyChanges {target: bottom4; opacity: 0.3}
                            PropertyChanges {target: header; comText: "Header 2"}
                            PropertyChanges {target: content; comText: "Content 2"}
                        },

                        State {
                            name: "third"
                            PropertyChanges {target: bottom1; opacity: 0.3}
                            PropertyChanges {target: bottom2; opacity: 0.3}
                            PropertyChanges {target: bottom4; opacity: 0.3}
                            PropertyChanges {target: header; comText: "Header 3"}
                            PropertyChanges {target: content; comText: "Content 3"}
                        },
                        State {
                            name: "forth"
                            PropertyChanges {target: bottom1; opacity: 0.3}
                            PropertyChanges {target: bottom2; opacity: 0.3}
                            PropertyChanges {target: bottom3; opacity: 0.3}
                            PropertyChanges {target: header; comText: "Header 4"}
                            PropertyChanges {target: content; comText: "Content 4"}
                        }
                    ]
                }
            }
        }
    }
}


