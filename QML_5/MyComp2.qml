import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3

Rectangle {
    property alias comWidth: main_rect.width
    property alias comHeight: main_rect.height
    property color comColor: "#d3d3d3"
    property color comBorderColor: "#dcdcdc"
    property alias comTextColor: text1.color
    property alias comText: text1.text

    id: main_rect
    width: parent.width
    height: parent.height * 0.1
    color: comColor
    border.color: comBorderColor

    Text {
        id: text1
        font.pointSize: 10
        font.weight: Font.DemiBold
        text: "Header"
        anchors.centerIn: parent
    }
}
